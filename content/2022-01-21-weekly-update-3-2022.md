+++
title = "Weekly Update (3/2022): First PINE64 Community Update and first Ubuntu Touch Q&A of 2022, some PinePhone keyboard videos, news on OpenBSD and Genode on the PinePhone."
date = "2022-01-21T21:00:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","PinePhone Pro","PinePhone Keyboard",]
categories = ["weekly update"]
authors = ["peter"]
+++

The third week of 2022 has the release of postmarketOS 21.12 Service Pack 1 and many smaller stories – a comparatively quiet week. 

<!-- more --> _Commentary in italics._ 


### Hardware news
* PINE64 published the January Community Update ([blog post](https://www.pine64.org/2022/01/15/january-update-more-news/), [video](https://tilvids.com/w/35XgkrMqrU8MME59pqH8aC)), without further highly exciting product announcements. This makes sense, as Chinese New Year is approaching, which means that operations (be it manufacturing, shipping or else) pretty much shut down for a bit. The PineNote Developer Edition is now available (for everyone), but ... even if you are an eInk enthusiast, you might still want to hold off if not a developer. _This time, PizzaLovingNerds video is especially good, so make sure to [watch it](#pine64-community-update)!_	
* Liliputing: [This kit turns a Raspberry Pi Zero 2 W into a pocket computer with a keyboard and display (crowdfunding soon)](https://liliputing.com/2022/01/this-kit-turns-a-raspberry-pi-zero-2-w-into-a-pocket-computer-with-a-keyboard-and-display-crowdfunding-soon.html). _Reminds me of the PocketC.H.I.P., nice._

### Fun projects
* [Using the PinePhone accelerometer as a device to keep you walking](https://twitter.com/utopiah/status/1351436027462881280).

### Software news
#### GNOME ecosystem
* [Chatty 0.6.0 beta has been released](https://source.puri.sm/Librem5/chatty/-/commit/30253118333ac52d3f137e76e063450cf1ffbfea) (with the latest stable release still being 0.4.0), bumping the version due to necessary changes that change the database schema and thus preventing easy downgrades.
* [Secrets 0.6.0 and 0.6.1, a subsequent bugfix release, where released this week.](https://gitlab.gnome.org/World/secrets/-/tags) _This is GNOME PasswordSafe gone GTK 4/libadwaita._
* [GNOME 42.alpha](https://mail.gnome.org/archives/devel-announce-list/2022-January/msg00004.html) has been released – the final release is going to happen in march, taking many apps to a new GTK4 base.
* This Week in GNOME: [#27 Borderless](https://thisweek.gnome.org/posts/2022/01/twig-27/).

#### Plasma/Maui ecosystem
* Phoronix: [Qt 6.3 Alpha Released With New Qt Quick Compiler For Commercial Customers](https://www.phoronix.com/scan.php?page=news_item&px=Qt-6.3-Alpha). _Sad to see speed improvements as a "commercial guys only" feature._
* Volker Krause: [KDE Frameworks 6 Continuous Integration](https://www.volkerkrause.eu/2022/01/15/kf6-continuous-integration.html). _Great!_
* Nate Graham: [The 15-Minute Bug Initiative](https://pointieststick.com/2022/01/18/the-15-minute-bug-initiative/). _Not mobile specific, but quite important, anyway!_
* Niccolò Ve: [KDE NEWS - Kirigami Improvements, New Multi-Monitor Tools, And More!](https://www.youtube.com/watch?v=XDufEXZSnL0).

#### Non-Linux
* Genode: [Road Map for 2022](https://genode.org/news/road-map-for-2022). _The road map includes the following: "we plan to take Genode on the Pinephone to a level where we can routinely use it for advanced applications, in particular video chat". Impressive plans!_
* ExoticSilicon.com: [Crystal installs OpenBSD on the Pinephone](https://www.exoticsilicon.com/crystal/pinephone_openbsd).

#### Distro releases
* [postmarketOS v21.12 Service Pack 1 has been released](https://postmarketos.org/blog/2022/01/17/v21.12.1-release/). _Great to see the Nokia N900 being back, Librem 5 GPS working now, the latest Sxmo and adding support for the PinePhone keyboard._ 

### Worth reading
#### Kernel building
* xnux.eu: [How to build megi's Pine­Phone kernel](https://xnux.eu/howtos/build-pinephone-kernel.html). _Megi added a how to, so if you've been holding off on building your own kernel, ..._

#### PINE64 Critisism
* Drew DeVault: [Pine64 should re-evaluate their community priorities](https://drewdevault.com/2022/01/18/Pine64s-weird-priorities.html). _This is an interesting read, and I agree that it would be great if PINE64 could do more with regard to Drew's first priority ("Implementing and upstreaming kernel drivers, u-Boot support, etc") to improve their devices software story.[^1] Regarding the rest, I think that unless PINE64 is switching to focus on phones exclusively or mostly (remember: They make SBCs, laptops and tablets, too), I am not too sure. I understand PINE64 as a "hardware provider", not a "product company" – it's up to the community to contribute (with time and work or financially) to the software development. If this is the case, this story could be told better or more openly – I've frequently seen people oder multiple PinePhones to support the effort in places where I felt that spending the same amount across a few software projects would have helped more. Assuming I am not completely wrong here, were just talking about the nightmare of funding FOSS projects properly once again._

  * [Related Reddit thread](https://teddit.net/r/PinePhoneOfficial/comments/s7ljs4/pine64_should_reevaluate_their_community/).
  * [Related Hacker News thread](https://news.ycombinator.com/item?id=30009452).

#### Keyboard 
* /u/GrumpyCat79: [What Desktop Environment do you guys use with the keyboard?](https://www.reddit.com/r/pinephone/comments/s3zgv4/what_desktop_environment_do_you_guys_use_with_the/)

#### Mainstream Publications write about the PinePhone Pro
* ArsTechnica: [The PinePhone Pro brings upgraded hardware to the Linux phone](https://arstechnica.com/gadgets/2022/01/the-pinephone-pro-brings-upgraded-hardware-to-the-linux-phone/).

#### postmarketOS on the SAMSUNG Galaxy S5
* project-insanity: [This is what you already can do with Linux on a low-budget smartphone](https://blog.project-insanity.org/2022/01/18/this-is-what-you-already-can-do-with-linux-on-a-low-budget-smartphone).

### Worth watching

#### PINE64 Community Update
* PINE64: [January Update: More News](https://tilvids.com/w/35XgkrMqrU8MME59pqH8aC).

#### PinePhone Keyboard
* Jose Briones: [Pinephone Keyboard Review](https://www.youtube.com/watch?v=TdRrxYqKeBE). 
* Izaic's Linux: [PinePhone Pro with Keyboard - Plasma Mobile Bug](https://www.youtube.com/watch?v=dnzcoAKbTN4).
*  (RTP) Privacy Tech Tips: [Pinephone Keyboard Case First Impression + Opensnitch Firewall + Faraday Bag #Quickie](https://www.youtube.com/watch?v=Xntd5lx2Av0).

#### UBports Ubuntu Touch
* UBports: [Ubuntu Touch Q&A 115](https://www.youtube.com/watch?v=P0ZAl-dFB4A). _This is a really good one, make sure to watch carefully in the beginning and listen until the end._
* Geotechland: [Ubuntu Touch OTA-21 New Updates](https://tilvids.com/w/jHVoLqnUvvH5ipdqGKkUuD).

#### Conference Talks
* linux.conf.au: ["A year in the life of GTK" - Emmanuele Bassi](https://www.youtube.com/watch?v=T9i5c7J29VU). _Great talk detailing the way towards and ideas behind GTK4!_

#### Shorts
* @utopiah: [postmarketOS Sxmo and PinePhone Keyboard](https://twitter.com/utopiah/status/1483572211407540229)

### Something missing?
If your projects' cool story is missing and you don't want that to happen again, please get in touch via social media or email!

[^1]: Let me also say that I mostly agree with his comments regarding Manjaro.
