+++
title = "Weekly #LinuxPhone Update (18/2022): Bluetooth Telephony with Linux Phones, Sxmo 1.9.0 and much more!"
date = "2022-05-09T22:15:00Z"
[taxonomies]
tags = ["PinePhone","PinePhone Pro","DanctNIX","Sailfish OS","Ubuntu Touch","Capyloon","Librem 5",]
categories = ["weekly update"]
authors = ["peter"]
+++
 
 A round of software news: New Chatty, Capyloon, DanctNIX releases, a Manjaro progress report, a new postmarketOS podcast episode and some more Linux App Summit talk recommendations!
<!-- more -->

_Commentary in italics._

### Vacatitorial II

It's monday, and we're back with what happened [while a break was taken](https://fosstodon.org/@linmob/108273418717307527). It's good to be back :)

### Software progress

#### GNOME ecosystem
* This Week in GNOME: [#42 Numerous Emojis](https://thisweek.gnome.org/posts/2022/05/twig-42/). _I wonder if that installer could be used in phones..._
* chergert: [Builder GTK 4 Porting, Part III](https://blogs.gnome.org/chergert/2022/05/07/builder-gtk-4-porting-part-iii/).
* [Chatty 0.6.4 has been released](https://source.puri.sm/Librem5/chatty/-/commit/d7c81acc0d7b67eff145e696e96c6a8590452d7c#9c96da0e9f91d7d8937b69b524702c106258f0d1_1_8), delivering contact blocking, bug fixes and more translations.

#### Plasma/Maui ecosystem
* Nate Graham: [This week in KDE: New features and many bugfixes for Plasma 5.25](https://pointieststick.com/2022/05/06/this-week-in-kde-new-features-and-many-bugfixes-for-plasma-5-25/).
* Vlad Zahorodnii: [What’s cooking in KWin? Plasma 5.25](https://blog.vladzahorodnii.com/2022/05/09/whats-cooking-in-kwin-plasma-5-25/). _Nice improvements!_
* Volker Krause: [March/April in KDE PIM](https://www.volkerkrause.eu/2022/05/04/kde-pim-march-april-2022.html).
* KDE.news: [Season of KDE 2022 - Conclusion](https://dot.kde.org/2022/05/03/season-kde-2022-conclusion).

#### Sxmo 
* [Sxmo 1.9.0 has been released](https://lists.sr.ht/~mil/sxmo-announce/%3C3FV34P1BUD8B6.34I97X4SKLKHH%40yellow-orcess.my.domain%3E). _Device profiles and Pipewire are huge, but there's more!_

#### Ubuntu Touch
* Jeroen Baten: [Your biweekly UBports news letter is here, once again](http://ubports.com/blog/ubports-news-1/post/your-biweekly-ubports-news-letter-is-here-once-again-3846). _Elections, new tutorials and apps - give it a read!_
* Koen Vervloesem: [TET Update 3 aka 0011: The first public development meeting was a success!](http://ubports.com/blog/ubports-news-1/post/tet-update-3-aka-0011-the-first-public-development-meeting-was-a-success-3844). _TET is the UBports Training Initiative._

#### Sailfish OS
*  flypig: [Sailfish Community News, 5th May, Documenting Sailfish](https://forum.sailfishos.org/t/sailfish-community-news-5th-may-documenting-sailfish/11373). _Nice roundup!_

#### Capyloon
* Capyloon, [the Boot2Gecko continuation, have published a new bunch of images](https://capyloon.org/releases.html#may-09) today. New features include more options for the share menu and a save to IPFS API.

#### Distro news
* [DanctNIX has seen another release](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20220502), bringing you lots of new packages and the requirement to install Tow-Boot on the PinePhone Pro beforehand.
* [Dan Johansen has shared another monthly roundup of what happened in Manjaro ARM](https://blog.strits.dk/this-month-in-manjaro-april-2022/).

#### Apps
* Alberto Mardegan: [Mitubo 0.9: multiple concurrent video downloads](http://www.mardy.it/blog/2022/05/mitubo-09-multiple-concurrent-video-downloads.html).

#### Kernel
* Martin Kepplinger for Purism: [Purism and Linux 5.18](https://puri.sm/posts/purism-and-linux-5-18/).



### Worth reading

#### Something huge for Bluetooth Telephony Afficionados
* Dylan van Assche: [Bluetooth HFP support in PulseAudio with ModemManager](https://dylanvanassche.be/blog/2022/bluetooth-hfp-linux-mobile/). _While I have little to no use for this, I love to have something to point "but it does not work with my car" persons to now._

#### Sxmo
* Slatecave.net: [Integrating swaylock-mobile with Sxmo 2/?](https://slatecave.net/blog/2022-05-07_integrating-my-screenlocker-with-sxmo-part-2/). _For reference: [Part 1](https://slatecave.net/blog/2022-03-18_integrating-my-screenlocker-with-sxmo/)._

#### Cameras are complicated
* Dorota Czaplejewicz for Purism: [Cameras: It’s Complicated](https://puri.sm/posts/cameras-its-complicated/).

#### AVMultiPhone
* TuxPhones: [AVMultiPhone is a peculiar MATE-on-phone desktop spin](https://tuxphones.com/avmultiphone-mate-desktop-pinephone-mobile-linux/).

#### Actual travelling to LAS
* Volker Krause: [Linux App Summit 2022](https://www.volkerkrause.eu/2022/05/07/las-2022-recap.html).
* Sam Thursfield: [Linux App Summit 2022](https://samthursfield.wordpress.com/2022/05/03/linux-app-summit-2022/).

#### FOSS ftw!
* Anjandev Momi: [Why we must insist on Free Network Services](https://momi.ca/posts/2022-05-01-freeNetwork.html).

#### Librem 5 or PinePhone Pro
* Anthony J. Martinez: [Mobile Linux - Update](https://ajmartinez.com/tech/posts/202218-001-mobile-linux-2). _At this point I'd make the same choice._

#### Librem 5 Field Trial
* Jan Vlug: [One week Librem 5 field trial – day 1](http://janvlug.org/blog/one-week-librem-5-field-trial-day-1/), [day 2](http://janvlug.org/blog/one-week-librem-5-field-trial-day-2/), [day 3](http://janvlug.org/blog/one-week-librem-5-field-trial-day-3/), [day 4](http://janvlug.org/blog/one-week-librem-5-field-trial-day-4/). _Nice series of reports!_

#### Digital markets discourage competition
* Graham Lee: [On The Duopoly Of Mobile](https://deprogrammaticaipsum.com/on-the-duopoly-of-mobile/). _Mobile Linux is not well represented, but it's still worth a read. Discussion on [hn](https://news.ycombinator.com/item?id=31236226)._

#### A route to convergence for devices that don't support it
* Gunnar Wolf: [Using a RPi as a display adapter](https://gwolf.org/2022/05/using-a-rpi-as-a-display-adapter.html). _Nice guide that imho should apply to other, e.g. Snapdragon 845 based devices._

### Worth listening
* postmarketOS podcast: [#17 INTERVIEW: Peter Mack (of LinuxPhoneApps, LINMOB.net Fame)](https://cast.postmarketos.org/episode/17-Interview-Peter-Mack-LinuxPhoneApps-LINMOB/). _First let me apologies for the issues I had speaking first (I was a bit ill), then thank the postmarketOS team for having me, the great conversation we had during this epidode (+ before and after) and Martijn Braam especially for editing!_


### Worth watching

#### Kali on PinePhone
* fossfrog: [Kali Linux Phosh port to Pinephone | fossfrog](https://www.youtube.com/watch?v=4_wAUizB2m8). _Looks nice! I wish I had the time for a distro review video of this (or any other distro), but ... no time._

#### Poco F1 Multi boot
* Alex Pav: [Poco F1 Multiboot](https://www.youtube.com/watch?v=gOMUqic2Fwk). _Wow!_

#### More Linux App Summit Talks
* Jorge Castro and Robert Mc Queen: [Flathub: now and next](https://tube.kockatoo.org/w/4LEZcE7Yj2AmnXtcVoSyb8?start=55m). _Lots of great features coming. Do watch this, especially if you generally are not a fan, it might help understand the "why"._
* Phaedrus Leeds: [Integrating Progressive Web Apps in GNOME](https://tube.kockatoo.org/w/4LEZcE7Yj2AmnXtcVoSyb8?start=7h14m30s)_This is the repo of [web apps|(https://gitlab.gnome.org/mwleeds/gnome-pwa-list), do add some if you use more._
* Joseph De Veaugh-Geiss: [Energy Conservation and Energy Efficiency with Free Software](https://www.youtube.com/watch?v=HxM15UOVmyA&t=17925s). _It matters!_

#### Installing Ubuntu Touch
* CyberPunked: [Google Pixel 3a (sargo) Ubuntu Touch Installation / Downgrade to Android 9 on Windows (2022-03-05)](https://www.youtube.com/watch?v=CdNGSdYCaDY).

#### Shorts 
* Scientific Perspective: [Waydroid | Ubuntu Touch GNU/Linux | POCO F1 #shorts](https://www.youtube.com/shorts/NKr19tOduHc). _Nice!_

### Something missing?
If your projects' cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!
