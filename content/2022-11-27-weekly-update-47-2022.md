+++
title = "Weekly GNU-like Mobile Linux Update (47/2022): Ubuntu Touch OTA 24, Maui and Nemo Mobile progress reports"
date = "2022-11-27T23:03:00Z"
draft = false
[taxonomies]
tags = ["Sailfish OS","Ubuntu Touch","Nemo Mobile","Maui","postmarketOS","open source modem firmware",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra  = " (with friendly assistance from plata's awesome script)"
+++

Also: biktorgj's PinePhone Modem Firmware gets even more features, SailfishOS gets support for the PinePhone keyboard, a new, but familiar Mastodon client and a lot more!

<!-- more -->

_Commentary in italics._


### Software progress

#### PinePhone Modem Firmware
- biktorgj Modem Firmware: [0.7.1 - Record this call](https://github.com/the-modem-distro/pinephone_modem_sdk/releases/tag/0.7.1)

#### GNOME ecosystem
- This Week in GNOME: [#71 Increased Circle](https://thisweek.gnome.org/posts/2022/11/twig-71/)
- Sébastien Wilmet: [gedit crowdfunding](https://informatique-libre.be/swilmet/blog/2022-11-20-gedit-crowdfunding.html)

#### Plasma ecosystem	
- Nate Graham: [This week in KDE: Humongous UI improvements](https://pointieststick.com/2022/11/25/this-week-in-kde-humongous-ui-improvements/)
- MauiKit: [Maui 2.2.1 Release](https://mauikit.org/blog/maui-2-2-1-release/)
- jbb: [Recent AudioTube improvements](https://jbb.ghsq.ga/2022/11/25/audiotube-improvements.html)
- Qt blog: [Qt Creator 9 released](https://www.qt.io/blog/qt-creator-9-released)
- Nate Graham: [A better fundraising platform](https://pointieststick.com/2022/11/23/a-better-fundraising-platform/)
- Nate Graham: [KDE is hiring a software engineer](https://pointieststick.com/2022/11/21/kde-is-hiring-a-software-engineer/)

#### Nemo Mobile
- Jozef Mlích: [Nemomobile in November/2022](https://blog.mlich.cz/2022/11/nemomobile-in-november-2022/)

#### Ubuntu Touch
- UBports News: [Ubuntu Touch OTA-24 Release](http://ubports.com/blog/ubports-news-1/post/ubuntu-touch-ota-24-release-3872)
  - Phoronix: [Ubuntu Touch OTA-24 Released - Still Powered By Ubuntu 16.04](https://www.phoronix.com/news/Ubuntu-Touch-OTA-24)
- UBports News: [From the UBports Newsletter Team](http://ubports.com/blog/ubports-news-1/post/from-the-ubports-newsletter-team-3870)

#### Sailfish OS
- [Adam Pigg on Twitter: "Well, it look less than a day to get that working. #pinephone #sailfishos"](https://twitter.com/adampigg/status/1595361551501885441) _This is about the PinePhone Keyboard accessory._

#### Sxmo
- [Sxmo 1.12.0 released — sourcehut lists](https://lists.sr.ht/~mil/sxmo-announce/%3C87y1sbw6jd.fsf%40momi.ca%3E) _Missed this last week._

#### Distributions
- Breaking updates in pmOS edge: [Mesa 22.2.4 update broke various apps on rk3399 devices](https://postmarketos.org/edge/2022/11/25/mesa-panfrost-breakage/)
- Manjaro PinePhone Phosh: [Beta 28](https://github.com/manjaro-pinephone/phosh/releases/tag/beta28)
- Alpine Linux: [Alpine 3.17.0 released](https://www.alpinelinux.org/posts/Alpine-3.17.0-released.html). _Looking forward to postmarketOS 22.12 :D_

#### Stack
- Phoronix: [Mesa 22.3-rc4 Brings Many Fixes, Official Release Now Expected Next Week](https://www.phoronix.com/news/Mesa-22.3-rc4-Released)
- Phoronix: [SDL3 Begins Dumping A Lot Of Old Code: GLES1, OS/2, DirectFB, WinRT, NaCl & More](https://www.phoronix.com/news/SDL3-Dumping-Old-Code)
- Phoronix: [Sway 1.8-rc1 Wayland Compositor Brings More Secure Screen Lockers, Improved Vulkan Code](https://www.phoronix.com/news/Sway-1.8-rc1-Released)
- Phoronix: [Wayland Protocol Finally Ready For Fractional Scaling](https://www.phoronix.com/news/Wayland-Fractional-Scale-Ready)
- Phoronix: [Mesa's Raspberry Pi V3DV Vulkan Driver Lands New Occlusion Queries Implementation](https://www.phoronix.com/news/V3DV-New-Occlusion-Queries)
- Phoronix: [Qualcomm Snapdragon 8 Gen 2 Patches Already Surfacing For The Linux Kernel](https://www.phoronix.com/news/Snapdragon-8-Gen-2-Linux)


#### Matrix
- Matrix.org: [This Week in Matrix 2022-11-25](https://matrix.org/blog/2022/11/25/this-week-in-matrix-2022-11-25)


### Worth noting
- [LinuxPhoneApps.org: "Testing Tooth (https://github.com/GeopJr/Tooth), continuation of the discontinued GTK mastodon client Tootle."](https://linuxrocks.online/@linuxphoneapps/109410081877203399). _GeopJr wants to fix some things before distributing this wider, and I am positive that work on open issues is appreciated, if you want to and can help out!_
- [lapoor: "They ported PostmarketOS to the nokia 8110 4G... it looks so derpy running XFCE. I guess I found another project"](https://outerheaven.club/notice/APtkjVszvXlMUkyYTo). _Who does not love the Banana Phone with [postmarketOS](https://wiki.postmarketos.org/wiki/Nokia_8110_4G_(nokia-argon))?_



### Worth reading
- adamd: [My experience with Linux phones](https://adamd.sdf.org/computers/mini/2022/10/14/LinuxPhones.html)
- LINMOB.net: [How about a more libre PinePhone with atk9k_htc?](https://linmob.net/how-about-more-libre-pinephone-with-ath9k-htc/)
- Purism: [What We Are Thankful For](https://puri.sm/posts/what-we-are-thankful-for/) _Fluff._
- Purism: [How Purism is Advancing Made in USA Electronics](https://puri.sm/posts/how-purism-is-advancing-made-in-usa-electronics/) _Double fluff._
- Neil Brown: [The fediverse, harassment, and moderation](https://neilzone.co.uk/2022/11/the-fediverse-harassment-and-moderation)


### Worth watching

- Phalio: [PinePhone + Keyboard with Charger/Dock in Phone](https://tube.tchncs.de/w/x5xW5LqNZqDkh616xhNFGR)
- Gardiner Bryant | TLG: [You're not ready for these Linux-powered smartphones.](https://www.youtube.com/watch?v=TccqlhRMkoc) _Well done!_
- g206: [Sailfish X on Xperia 10 ii](https://www.youtube.com/watch?v=7QYmi6P_ans). _Japanese._
- knot126: [postmarketOS on Fire HD 8 2018](https://www.youtube.com/watch?v=U0yd2hzt9_o)
- ItzVlλdik: [postmarketOS on Samsung Galaxy J3 | Edge, LXQt, Openbox](https://www.youtube.com/watch?v=UP8XWJ1hEG4)

### Thanks

Huge thanks again to Plata for [a nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A) for the next one!

PS: I'm looking for feedback - [what do you think?](mailto:weekly-update@linmob.net?subject=Feedback%20on%20Weekly%20Update)

